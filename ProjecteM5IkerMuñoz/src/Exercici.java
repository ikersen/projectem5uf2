import java.util.Scanner;
/**
 * Un programa que et respon si amb el vehicle, via i velocitat vas correctament.
 * 
 * @version 1
 * @author Iker Mu�oz
 */
public class Exercici {
	/**
	 * El main ho gestiona tot, controla la entrada dels usuaris.
	 * Depenent del que els usuaris introduiexin, el programa resp�n una cosa o un altre.
	 * del joc.
	 */
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		String vehicle="";
		String via="";
		int velocitat=0;
		System.out.println("Introdueix el vehicle (Turisme|Camio|Ciclomotor): ");
		vehicle=reader.next();
		System.out.println("Introdueix la via(Urbana|Convencional|Autopista): ");
		via=reader.next();
		System.out.println("Introdueix la velocitat: ");
		velocitat=reader.nextInt();
		vehicle.toLowerCase();
		via.toLowerCase();
		
		if(vehicle.equals("turisme")) {
			if(via.equals("urbana")) {
				if(velocitat>50 || velocitat<25) {
					System.out.println("VAS MALAMENT");
				}else {
					System.out.println("VAS B�");
				}
			}else if(via.equals("convencional")) {
				if(velocitat>90 || velocitat<45) {
					System.out.println("VAS MALAMENT");
				}else {
					System.out.println("VAS B�");
				}
			}else if(via.equals("autopista")) {
				if(velocitat>120 || velocitat<60) {
					System.out.println("VAS MALAMENT");
				}else {
					System.out.println("VAS B�");
				}
			}else {
				System.out.println("No has escrit b�");
			}
		}else if(vehicle.equals("camio")) {
			if(via.equals("urbana")) {
				if(velocitat>50 || velocitat<25) {
					System.out.println("VAS MALAMENT");
				}else {
					System.out.println("VAS B�");
				}
			}else if(via.equals("convencional")) {
				if(velocitat>70 || velocitat<35) {
					System.out.println("VAS MALAMENT");
				}else {
					System.out.println("VAS B�");
				}
			}else if(via.equals("autopista")) {
				if(velocitat>90 || velocitat<60) {
					System.out.println("VAS MALAMENT");
				}else {
					System.out.println("VAS B�");
				}
			}else {
				System.out.println("No has escrit b�");
			}
		}else if(vehicle.equals("ciclomotor")) {
			if(via.equals("urbana")) {
				if(velocitat>45 || velocitat<25) {
					System.out.println("VAS MALAMENT");
				}else {
					System.out.println("VAS B�");
				}
			}else if(via.equals("convencional")) {
				if(velocitat>45) {
					System.out.println("VAS MALAMENT");
				}else {
					System.out.println("VAS B�");
				}
			}else if(via.equals("autopista")) {
				System.out.println("VAS MALAMENT");
			}else {
				System.out.println("No has escrit b�");
			}
		}else {
			System.out.println("No has escrit b�");
		}
		reader.close();
	}
}