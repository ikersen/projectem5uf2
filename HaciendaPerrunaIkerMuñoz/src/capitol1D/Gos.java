package capitol1D;

import java.util.Random;

public class Gos {
	private int edat;
	private String nom;
	private int numFills;
	private char sexe;
	private Ra�a ra�a;
	private GosEstat estat;

	private static int comptador = 0;

	//Constructors de la classe Gos
	
	Gos (){
		nom = "Sense nom";
		edat = 4;
		numFills = 0;
		sexe = 'M';
		estat = GosEstat.VIU;
		ra�a = null; //no cal posar-ho, per defecte, si no posem valor ser null
		comptador ++;
	}

	Gos (String nom, int edat, int numFills, char sexe){
		this.nom = nom;
		this.edat = edat;
		this.numFills = numFills;
		this.sexe = sexe;
		this.estat = GosEstat.VIU;
		comptador ++;
	}

	Gos (String n){
		this();
		nom = n;
	}

	Gos (Gos g){
		nom = g.getNom();
		edat = g.getEdat();
		numFills = g.getNumFills();
		sexe = g.getSexe();
		estat = g.getEstat();
		ra�a = g.getRa�a();
		comptador ++;
	}

	//Constructor amb tots els parmetres. Incls la raa. Fa servir altre constructor
	Gos (String nom, int edat, int numFills, char sexe, Ra�a r) {

		this (nom, edat, numFills, sexe);
		ra�a = r;
	}

	//Mtodes setters i getters dels atributs de Gos
	
	public int getNumFills() {
		return numFills;
	}

	public char getSexe() {
		return sexe;
	}

	public void setSexe(char sexe) {
		if (sexe == 'M' || sexe == 'F')
			this.sexe = sexe;
	}

	public void setEdat(int e) {
		this.edat = e;
		if (this.ra�a != null) {
			if (this.edat > this.ra�a.getTempsVida())
				this.estat = GosEstat.MORT;
		}
		else
			if(this.edat >= 10)
				this.estat = GosEstat.MORT;
	}

	public void setNom(String n) {
		nom = n;
	}

	public int getEdat() {
		return edat;
	}

	public String getNom() {
		return nom;
	}

	public Ra�a getRa�a() {
		return ra�a;
	}

	public void setRa�a(Ra�a ra�a) {
		this.ra�a = ra�a;
	}

	public GosEstat getEstat() {
		return estat;
	}

	public void setEstat(GosEstat estat) {
		
		this.estat = estat;
	}

	//Comportament dels objectes de la classe Gos
	
	public void borda() {
		System.out.println("guau guau");
	}

	public String toString() {
		String res = "Nom: " + nom + " Edat: " + edat + " Num Fills: " + numFills + " Sexe: " + sexe  + " Estat: " + estat;
		if (ra�a != null)
			res = res +  "\n"+ ra�a.toString() + "\n";
		return res;
	}
	
	public void visualitzar() {
		if (ra�a == null)
			System.out.println("Nom: " + nom + " Edat: " + edat + " Num Fills: " + numFills + " Sexe: " + sexe + " Estat: " + estat);
		else
			System.out.println("Nom: " + nom + " Edat: " + edat + " Num Fills: " + numFills + " Sexe: " + sexe +  " Estat: " + estat + "\n"+ ra�a.toString());
	}

	public void clonar(Gos g) {
		nom = g.getNom();
		edat = g.getEdat();
		estat = g.getEstat();
		numFills = g.getNumFills();
		sexe = g.getSexe();
		ra�a = g.getRa�a();
	}

	public static int quantsGossos() {
		return comptador;
	}
	
	
	public Gos aparellar(Gos g) {
		Gos fill;

		if (esPodenAparellar(this, g)) {
			fill = this.crearGosset(g);
			return fill;
		}
		else return null;
	}

	//Aquest mtode no forma part del comportament de l'objecte, per s es refereix a l'objecte que el crida (this). s privat
	private Gos crearGosset(Gos g) {
		// TODO Auto-generated method stub
		Random rnd = new Random();
		Gos mare, pare;
		char sexe;
		String nom;
		Ra�a rFill;
		
		//Averigem qui s el pare i qui s la mare
		if (this.getSexe() == 'F') {
			mare = this;
			pare = g;
		}
		else {
			mare = g;
			pare = this;
		}

		//Averigem el sexe
		if (rnd.nextInt(2) == 0) {
			sexe = 'M';
			nom = "Fill de " + pare.getNom();
		}
		else {
				sexe = 'F';
				nom = "Fill de " + mare.getNom();
		}

		//Obtenim la raa segons el pare i la mare
		rFill = obtenirRa�a(mare.getRa�a(), pare.getRa�a());
		
		//Incrementem el nmero de fills dels dos progenitors
		this.numFills++;
		g.numFills++;
		
		//Creem el gosset amb tots els atributs i el retornem
		return new Gos(nom,0,0,sexe,rFill);
	}

	//Segons el pare i la mare averigem la raa del gosset
	private static Ra�a obtenirRa�a(Ra�a ra�am, Ra�a ra�ap) {
		
		//raam: raa de la mare i raap: raa del pare
		if (ra�am == null) return ra�ap;
		
		if (ra�ap == null) return ra�am;
		
		if (ra�am.getDominant())
				 return ra�am;
		else {
				if (ra�ap.getDominant())
					return ra�ap;
				else 
					return ra�am;
		}
	}

	//Retorna un boole indicant si es donen les condicions per que s'apareguin els dos gossos o no segons el que indica l'enunciat
	private static boolean esPodenAparellar(Gos g1, Gos g2) {
		// TODO Auto-generated method stub
		Gos mare, pare;
		boolean res;
		
		if (g1.getSexe() == g2.getSexe()) return false;
		
		if (g1.getEstat() != GosEstat.VIU || g2.getEstat() != GosEstat.VIU) return false;
		
		if ((g1.getEdat() < 2 || g1.getEdat() > 10) || (g2.getEdat() < 2 && g2.getEdat() > 10)) return false;
			
		if (g1.getSexe() =='F') {
				mare = g1;
				pare = g2;
		}
		else {
				mare = g2;
				pare = g1;
		}
				
		if (mare.getNumFills() >= 3) return false;
		
		if (mare.ra�a == null || pare.ra�a == null) return true;
		
		if (ra�aPossible(mare.getRa�a(), pare.getRa�a()) )	return true;
		else return false;

	}

	//Retorna un boole indicant si segon la raa de cada progenitor, pot haver-hi un gosset o no.
	private static boolean ra�aPossible(Ra�a ra�am, Ra�a ra�ap) {
		// TODO Auto-generated method stub
		boolean res;
		
		switch (ra�am.getMida()) {
		 	case PETIT: res = ra�ap.getMida() == GosMida.PETIT;
		 				break;
		 	case MITJA: res = ra�ap.getMida() == GosMida.PETIT || ra�ap.getMida() == GosMida.MITJA;
		 			    break;
		 	default: res = true;
		}
	
		
		return res;
	}
	
	public void incEdat() {
		if (getEstat()==GosEstat.VIU) {
			edat++;
			actualitzarEstat();
		}
	}
	
	private void actualitzarEstat() {
		if (getRa�a()!=null) {
			if (edat >= this.getRa�a().getTempsVida())
				estat = GosEstat.MORT;
		}
		else //si no te raa ens el "carreguem" quan passa de 10 anys
			if (edat >10)
				estat = GosEstat.MORT;
	}

}
