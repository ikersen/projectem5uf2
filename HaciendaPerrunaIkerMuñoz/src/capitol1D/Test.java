package capitol1D;

import java.util.Scanner;

public class Test {
	
		public static void main(String[] args) {
			// TODO Auto-generated method stub
			/*Raa caniche = new Raa ("Caniche", GosMida.PETIT, 15, false);
			Gos sulta = new Gos("Sulta");	
			Gos pepelotas = new Gos("Milu",1, 40, 'M',caniche);
			Gos dolly = new Gos(pepelotas);
			Gos puchi = new Gos("puchi simpson");
			Gos calimero = new Gos();
			
			Gos laika = dolly;
			puchi.visualitzar();
			calimero.visualitzar();
			
			puchi.clonar(dolly);

			pepelotas.visualitzar();
			dolly.visualitzar();
			laika.visualitzar();
			puchi.visualitzar();

			dolly.setNom("dolly patrow");
			pepelotas.visualitzar();
			dolly.visualitzar();  //dolly i laika apunten al mateix objecte
			laika.visualitzar();
			puchi.visualitzar();
			
			System.out.println("S'han generat: " + Gos.quantsGossos());
			*/
			
			//Programa Apartat D
			
			Gos gosset = null;;
			Granja granja1 = new Granja();
			granja1 = GenerarGranja();
			granja1.visualitzar();
			
			Granja granja2 = new Granja();
			granja2 = GenerarGranja();
			granja2.visualitzar();
			
			Granja granja3 = new Granja();
			
			for (int i = 0; i < granja1.getNumGosos(); i++) {
				gosset = granja1.obtenirGos(i).aparellar(granja2.obtenirGos(i));
				granja3.afegir(gosset);
			}
			
			granja3.visualitzar();
			
			
		}
		
		
		
		
		static Granja GenerarGranja() {
			Granja gr;
			String nom, nomRa�a;
			int edat, num, numGossos;
			char sexe;
			Scanner reader = new Scanner(System.in);
			Ra�a r;
			int grandaria;
			GosMida gm = null;
			
			System.out.print("Quants gossos vols guardar a la granja?: ");
			gr = new Granja(reader.nextInt());
			reader.nextLine();
			
			for (int i = 0; i < gr.getTopGossos(); i++) {
				System.out.println("Dades del gos " + (i+1) + ": ");
				System.out.print("Nom: ");
				nom = reader.nextLine();
				System.out.print("Edat: ");
				edat = reader.nextInt();
				System.out.print("NumFills: ");
				num = reader.nextInt();
				System.out.print("Sexe: ");
				sexe = reader.next().charAt(0);
				reader.nextLine();
				System.out.print("Ra�a: ");
				nomRa�a = reader.nextLine();
				
				System.out.print("Grandria : 1. GRAN, 2. MITJA, 3. PETIT:   ");
				switch(reader.nextInt()) {
				case 1: gm = GosMida.GRAN;
						break;
				case 2: gm = GosMida.MITJA;
						break;
				case 3: gm = GosMida.PETIT;
				}
				reader.nextLine();
				
				r = new Ra�a (nomRa�a, gm);
				
				System.out.print("Ra�a dominant? (s/n) ");
				if (reader.next().charAt(0) == 's') 
					r.setDominant(true);
				reader.nextLine();
				
				numGossos = gr.afegir(new Gos(nom, edat, num, sexe , r));	
				//Gos g = new Gos(nom, edat, num, sexe , r));	
				//numGossos = gr.afegir(g);
				if (numGossos != -1)
					System.out.println ("S'acaba d'afegir el gos n�mero: " + gr.getNumGosos());
				else
					System.out.println ("La granja est plena. No s'ha pogut afegir el gos");
			}
			
			return gr;
		}
		
	}
