package capitol1C;

class Granja {
	private Gos[] gossos;
	private int numGossos;
	private int topGossos;
	
	public Granja() {
		numGossos = 0;
		topGossos = 100;
		gossos = new Gos[topGossos];
	}
	public Granja(int t) {
		if(t>100 || t<1) {
			t=100;
		}
		topGossos=t;
		gossos = new Gos[topGossos];
	}
	public Gos obtenirGos(int i) {
		if(i<numGossos) {
			return gossos[i];
		}
		else {
			return null;
		}
	}
	public Gos[] getGossos() {
		return gossos;
	}
	public int getNumGossos() {
		return numGossos;
	}
	public int getTopGossos() {
		return topGossos;
	}
	public int afegir(Gos s) {
		if(numGossos<topGossos) {
			gossos[numGossos]=s;
			numGossos++;
			return numGossos;
		}
		else {
			return -1;
		}
	}
	public String toString() {
		String mostra = "";
		for(int i = 0; i<numGossos;i++) {
			mostra += gossos[i].toString()+"\n";
		}
		return mostra;
	}
	public void visualitzar() {
		System.out.println(this.toString());
	}
	public void visualitzarVius() {
		String mostravius = "";
		for(int i = 0; i<numGossos;i++) {
			if(gossos[i].getEstat()==GosEstat.VIU) {
				mostravius += gossos[i].toString()+"\n";
			}
		}
		System.out.println(mostravius);
	}
}
