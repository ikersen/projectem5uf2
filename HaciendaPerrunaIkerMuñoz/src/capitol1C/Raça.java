package capitol1C;

public class Ra�a {
	///Atributs
	private String nomRa�a;
	private GosMida mida;
	private int tempsVida;
	private boolean dominant;
	
	///Constructors
	public Ra�a(String nom, GosMida gt, int t) {
		this(nom, gt);
		tempsVida=t;
	}
	
	public Ra�a(String nom, GosMida mida) {
		nomRa�a=nom;
		this.mida=mida;
		tempsVida=10;
		dominant=false;
	}
	
	//M�todes accesors
	public String getRa�a() {
		return nomRa�a;
	}
	public GosMida getMida() {
		return mida;
	}
	public int getTempsVida() {
		return tempsVida;
	}
	public boolean getDominant() {
		return dominant;
	}
	public void setRa�a(String n) {
		nomRa�a=n;
	}
	public void setMida(GosMida m) {
		mida=m;
	}
	public void setTempsVida(int t) {
		tempsVida=t;
	}
	public void setDominant(boolean d) {
		dominant=d;
	}
	
	//Altres m�todes
	public String toString() {
		return "Ra�a="+ nomRa�a+" \nMida="+mida+" \nTemps de Vida="+tempsVida+" \nDominant="+dominant;
	}
}
