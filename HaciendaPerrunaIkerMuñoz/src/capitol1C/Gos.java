package capitol1C;

public class Gos {
			///Atributs
			private int edat ;
			private String nom ;
			private int fills ;
			private char sexe ;
			private static int quantitatGossos=0; 
			private Ra�a ra�a;
			private GosEstat estat= GosEstat.VIU;
			///Constructors
			public Gos(int e, String n, int f, char s, Ra�a r){
				this(e,n,f,s);
				ra�a = r;
			}
			public Gos() {
				quantitatGossos++;
				edat=4;
				nom="SenseNom";
				fills=0;
				sexe='M';
			}
			
			public Gos(int e, String n, int f, char s) {
				quantitatGossos++;
				edat=e;
				nom=n;
				fills=f;
				sexe=s;
			}
			
			///Constructors c�pia
			
			public Gos(Gos copia) {
				quantitatGossos++;
				this.edat=copia.getEdat();
				this.nom=copia.getNom();
				this.fills=copia.getFills();
				this.sexe=copia.getSexe();
			}
			
			public Gos(String n) {
				this();
				nom=n;
			}
			//M�todes accesors
			public int getEdat() {
				return edat;
			}
			public String getNom() {
				return nom;
			}
			public int getFills() {
				return fills;
			}
			public char getSexe() {
				return sexe;
			}
			public Ra�a getRa�a() {
				return ra�a;
			}
			public GosEstat getEstat() {
				return estat;
			}
			public void setEdat(int e) {
				edat=e;
				if(ra�a != null) {
					if(edat>ra�a.getTempsVida()) {
						estat = GosEstat.MORT;
					}
				}
				else {
					if(edat>9) {
						estat = GosEstat.MORT;
					}
				}
			}

			public void setNom(String n) {
				nom=n;
			}
			public void setFills(int f) {
				fills=f;
			}
			public void setSexe(char s) {
				sexe=s;
			}
			public void setRa�a(Ra�a r) {
				ra�a=r;
			}
			public void setEstat(GosEstat est) {
				estat=est;
			}
			
			//Altres m�todes
			public void borda() {
				System.out.println("guau guau");
			}
			
			public void visualitzar() {
				System.out.println(toString());
			}
			
			public String toString() {
				if(ra�a != null) {
					return "Edat="+ edat+" \nNom="+nom+" \nFills="+fills+" \nSexe="+sexe+" \nRa�a="+ra�a.getRa�a();
				}else {
					return "Edat="+ edat+" \nNom="+nom+" \nFills="+fills+" \nSexe="+sexe;
				}
				
			}
			public void clonar(Gos copia) {
				this.edat=copia.getEdat();
				this.nom=copia.getNom();
				this.fills=copia.getFills();
				this.sexe=copia.getSexe();
			}
			static int quantitatGossos() {
				return quantitatGossos;
			}
}
