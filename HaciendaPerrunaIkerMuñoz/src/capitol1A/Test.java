package capitol1A;

import java.util.Scanner;

public class Test {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		System.out.println("Gos 1 per defecte:");
		Gos gos1= new Gos();
		gos1.borda();
		gos1.visualitzar();
		System.out.println();
		
		System.out.println("Gos 2 manualment:");
		System.out.print("Introdueix la edat: ");
		int edat=reader.nextInt();
		System.out.print("Introdueix el nom: ");
		String nom=reader.next();
		System.out.print("Introdueix els fills: ");
		int fills=reader.nextInt();
		System.out.print("Introdueix el sexe (M o F): ");
		char sexe=reader.next().charAt(0);
		
		Gos gos2= new Gos(edat, nom, fills, sexe);
		gos2.borda();
		gos2.visualitzar();
		System.out.println();
		
		System.out.println("Gos 3 copiat del Gos 2 fent servir contructor c�pia:");
		Gos gos3= new Gos(gos2);
		gos3.borda();
		gos3.visualitzar();
		System.out.println();
		
		System.out.println("Gos 4 copiat del Gos 1 fent servir el m�tode clonar:");
		Gos gos4= new Gos();
		gos4.clonar(gos1);
		gos4.borda();
		gos4.visualitzar();
		System.out.println();
		
		System.out.println("De moment tenim "+Gos.quantitatGossos()+" gossos creats");
		reader.close();
		}
	

}
