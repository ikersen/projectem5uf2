package capitol1A;

public class Gos {
			///Atributs
			private int edat ;
			private String nom ;
			private int fills ;
			private char sexe ;
			private static int quantitatGossos=0; 
			///Constructors
			public Gos() {
				quantitatGossos++;
				edat=4;
				nom="SenseNom";
				fills=0;
				sexe='M';
			}
			
			public Gos(int e, String n, int f, char s) {
				quantitatGossos++;
				edat=e;
				nom=n;
				fills=f;
				sexe=s;
			}
			
			///Constructors c�pia
			
			public Gos(Gos copia) {
				quantitatGossos++;
				this.edat=copia.getEdat();
				this.nom=copia.getNom();
				this.fills=copia.getFills();
				this.sexe=copia.getSexe();
			}
			
			public Gos(String n) {
				this();
				nom=n;
			}
			//M�todes accesors
			public int getEdat() {
				return edat;
			}
			public String getNom() {
				return nom;
			}
			public int getFills() {
				return fills;
			}
			public char getSexe() {
				return sexe;
			}
			public void setEdat(int e) {
				edat=e;
			}
			public void setNom(String n) {
				nom=n;
			}
			public void setFills(int f) {
				fills=f;
			}
			public void setSexe(char s) {
				sexe=s;
			}
			
			//Altres m�todes
			public void borda() {
				System.out.println("guau guau");
			}
			
			public void visualitzar() {
				System.out.println(toString());
			}
			
			public String toString() {
				return "Edat="+ edat+" \nNom="+nom+" \nFills="+fills+" \nSexe="+sexe;
			}
			public void clonar(Gos copia) {
				this.edat=copia.getEdat();
				this.nom=copia.getNom();
				this.fills=copia.getFills();
				this.sexe=copia.getSexe();
			}
			static int quantitatGossos() {
				return quantitatGossos;
			}
}
