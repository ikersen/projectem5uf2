package capitol1E;
import java.util.Random;
import java.util.Scanner;

public class Test {

		static Scanner sc = new Scanner(System.in);
		static Random rnd = new Random();
		
		public static void main(String[] args) {
			// TODO Auto-generated method stub
			int cicles=0;
			
			int valor;
			int total;
			Gos g1;
			Gos g2;
			Gos gfill;
			Gos gaux = new Gos();
			int j;
			
			cicles = obtenirAnys();
			
			valor = 10;
			Granja lMG1 = new Granja(100); // generar granja capacitat per a 100 gossos
			omplirGranja(lMG1, valor); //fiquem a granja 10 gossos aleatoris

			System.out.println("***** INICIALMENT *****");
			lMG1.visualitzar();

			//per cada cicle
			for(int vegades=0;vegades<cicles;vegades++) {
				total = lMG1.getNumGosos();
				//per cada gos
				j = 0;
				while (j<total-1) {
					g1 = lMG1.obtenirGos(j);
					g2 = lMG1.obtenirGos(j+1);
					gfill = g1.aparellar(g2);
					if (gfill!=null) { //xit. L'amor ha triomfat
						lMG1.afegir(gfill);
						g1.incEdat();
						g2.incEdat();
						j+=2;
					}
					else {	//fiasco. No hi ha "enteniment"
						//intercanvi de gossos (j, j+1)
						/*gaux = g1;
						g1 = g2;
						g2 = gaux;
						Incorrecte*/
						gaux.clonar(g1);
						g1.clonar(g2);
						g2.clonar(gaux);
						g2.incEdat();
						j++;
					}
				}
				System.out.println("***************************************");
				System.out.println("*****Passa any " + (vegades+1) + "*****");
				System.out.println("***************************************");
				lMG1.visualitzar();
				
			//	sc.nextLine();
				
				
				//espera 5 segons per visualitzar millor
				/*
				try {
					//Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}*/
				
			}
			
		}
		
		private static int obtenirAnys() {
			// TODO Auto-generated method stub
			int cicles = 0;
			
			do {
				System.out.println("Digues el n�mero d'anys que vols evolucionar (1..200)");
				try {
					cicles = sc.nextInt();
				}
				catch(Exception e) {
					System.out.println("Entrada incorrecta");
					sc.nextLine();
				}
			} while (cicles > 200 || cicles < 1);
			
			return cicles;
		}

		static void omplirGranja (Granja laMevaGranja, int ocupacio) {
			int limit = laMevaGranja.getTopGossos();
			int qRaa;
			Ra�a raaGos;
			int qedat;
			int resposta;
			int qSexe;
			
			//Definim 4 races fitxes
			Ra�a [] races = {
					new Ra�a("pastorAlemany",GosMida.GRAN, 20, true), 
					new Ra�a("yorshie",GosMida.PETIT, 24, false),
					new Ra�a("corgi",GosMida.PETIT, 25, false),
					new Ra�a("boxer",GosMida.MITJA, 18, true),
					null //No hi ha raa
			};
			
			// omplir granja de gos aleatoris
			for(int i=0;i<ocupacio;i++) {
				
				Gos g = new Gos();
				//assignaci aleatria de raa
				qRaa = rnd.nextInt(5);
				raaGos = races[qRaa];			
				g.setRa�a(raaGos);
				//tamb podriem posar directament g.getRaa(races[rnd.nextInt(5)]);
				
				//assignaci aleatria d'edat
				g.setEdat(rnd.nextInt(10));
				
				//assignaci el nom
				g.setNom("Gos " + (i+1));
				
				//assignaci del sexe
				qSexe = rnd.nextInt(2);
				if (qSexe==0)
					g.setSexe('M');
				else
					g.setSexe('F');
				
				//inserci del gos a la granja
				resposta = laMevaGranja.afegir (g);
				if (resposta == -1) {
					System.out.println("Error: no s'ha pogut afegir " + g.getNom());
				}
			}
		}
		
		
}
