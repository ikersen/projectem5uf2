package capitol1E;

import java.util.Arrays;
import java.util.Scanner;

public class Granja {
	private Gos [] gossos;
	private int numGossos;
	private int topGossos = 100;
	
	public Granja() {
		gossos = new Gos[topGossos];
		numGossos = 0;
	}
	
	public Granja (int top) {
		if (top >= 1 && top <= 100) {
			topGossos = top;
		}
		gossos = new Gos[topGossos];
		numGossos = 0;
	}

	public Gos[] getGossos() {
		return gossos;
	}
	
	public int getNumGosos() {
		return numGossos;
	}

	
	public int getTopGossos() {
		return topGossos;
	}

	public int afegir(Gos g) {
		if (numGossos == topGossos) 
			return -1;
		else {
			gossos[numGossos] = g;
			return numGossos++;
		}
	}

	@Override
	public String toString() {
		String res;
		
		res = "\nGossos de la granja\n\n";
		for (int i = 0; i < numGossos; i++)
			res = res + "\n" +   gossos[i].toString();
		return res;
	}
	
	public void visualitzar() {
		System.out.println(this.toString());
	}
	
	public void visualitzarVius() {
		for (int i = 0; i < numGossos; i++) {
			if (gossos[i].getEstat() == GosEstat.VIU) {
				gossos[i].visualitzar();
			}
		}
	}
	
	
	public Gos obtenirGos(int pos) {
		if (pos >= this.getTopGossos()) return null;
		else
			return this.gossos[pos];
	}
}
