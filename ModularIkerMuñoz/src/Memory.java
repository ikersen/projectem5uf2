import java.util.Scanner;
/**
 * Un programa que representa el joc popular del memory.
 * @version 1
 * @author Iker Mu�oz
 */

public class Memory {
	
	static Scanner reader = new Scanner(System.in);
	/**
	 * La mida del tauler est� fixada a 4x4 per aix� �s global
	 */
	static int midatauler=4;
	/**
	 * El n�mero m�xim de encerts per el tauler que tenim �s de 8 per aix� �s global.
	 */
	static int maxcorrecte=8;
	
	/**
	 * El main cridar� a la funci� joc que ser� la que gestionar� el funcionament general.
	 */

	public static void main(String[] args) {
		joc();

	}
	/**
	 * La funci� joc gestiona el joc sencer.
	 * 
	 * Inicialitza variables i crida a altres funcions per al correcte funciomnament del joc.
	 */
	static void joc() {
		char [][]tauler=new char[midatauler][midatauler];
		char [][]secret=new char[midatauler][midatauler];
		boolean torncorrecte=false;
		int acerts=0;
		int contj1=0;
		int contj2=0;
		int jugadortirada=1;
		inicialitzaTauler(tauler);
		mostraTauler(tauler);
		inicialitzaSecret(secret);
		while(acerts<maxcorrecte) {
			torncorrecte=false;
			System.out.println("Torn del jugador "+jugadortirada+":");
			torncorrecte=torn(tauler, secret);
			if(torncorrecte) {
				acerts++;
				if(jugadortirada==1) {
					contj1++;
				}else {
					contj2++;
				}
			}else {
				if(jugadortirada==1) {
					jugadortirada=2;
				}else {
					jugadortirada=1;
				}
			}
		}
		if(contj1>contj2) {
			System.out.println("Ha guanyat el jugador 1 amb "+contj1+" punts!");
		}else {
			System.out.println("Ha guanyat el jugador 2 amb "+contj2+" punts!");
		}
		
		
	/**
	 * La funci� �s cridada per inicialitzar el tauler sencer amb interrogants i reb un tauler buit.
	 * @param tauler �s la matriu que s'envia desde el joc i que s'omplir� tota a interrogants.
	 */
	}
	static void inicialitzaTauler(char[][]tauler) {
		for (int f = 0; f < midatauler; f++) {
			for (int c = 0; c < midatauler; c++) {
				tauler[f][c]='?';
			}
		}
	}
	/**
	 * La funci� �s cridada per mostrar el tauler que ha rebut.
	 * @param tauler Cont� el tauler omplert tot a interrogants.
	 */
	static void mostraTauler(char[][]tauler) {
		System.out.println("  0123");
		for (int f = 0; f < midatauler; f++) {
			System.out.print(f+" ");
			for (int c = 0; c < midatauler; c++) {
				System.out.print(tauler[f][c]);
			}
			System.out.println();
		}
	}
	/**
	 * La funci� crida altres funcions per inciar el tauler secret, que son: posaPeces i remenaPeces.
	 * 
	 * En aquest punt la matriu de secret est� buida a espera de omplirla amb aquesta funci�.
	 * @param secret Cont� el secret buit
	 */
	static void inicialitzaSecret(char[][]secret) {
		posaPeces(secret);
		remenaPeces(secret);
	}
	/**
	 * Reb una matriu secret buida, i la seva funci� �s omplir-la amb les lletres del abecedari
	 * de la A a la H, de dos en dos.
	 * 
	 * Exemple: AABB CCDD EEFF GGHH per� en format taula 4x4
	 * @param secret Cont� el secret buit i l'omple ordenat alfab�ticament.
	 */
	static void posaPeces(char[][]secret) {
		int x=0;
		for (int f = 0; f < midatauler; f++) {
			for (int c = 0; c < midatauler; c++) {
				if(c%2!=0) {
					secret[f][c]=(char)('A'+x);
					x++;
				}else {
					secret[f][c]=(char)('A'+x);
				}
			}
		}
	}
	/**
	 * Reb la matriu secret omplerta alfab�ticament i la remena aleat�riament
	 * @param secret Reb la matriu i remena el contingut.
	 */
	static void remenaPeces(char[][]secret) {
		int max = midatauler-1;
		int min = 0;
		int range = max - min + 1;
		char aux=0;
		int rndmcol=0;
		int rndmfil=0;
		
		for (int f = 0; f < midatauler; f++) {
			for (int c = 0; c < midatauler; c++) {
				rndmcol=(int) (Math.random() * range) + min;
				rndmfil=(int) (Math.random() * range) + min;
				aux=secret[f][c];
				secret[f][c]=secret[rndmfil][rndmcol];
				secret[rndmfil][rndmcol]=aux;
			}
		}

	}
	/**
	 * Aquesta funci� demana un n�mero sobre la posici� del tauler que vols destapar
	 * 
	 * Valida que el n�mero ficat sigui un enter i estigui entre el 0 i el 3 que son els n�meros que pot rebre.
	 * @return Retorna el n�mero de la posici� a on volem destapar, sent aquest n�mero v�lid.
	 */
	static int validarDada() {
		int i=0;
		boolean numcorrecte = false;
		while (!numcorrecte) {
			if (reader.hasNextInt()) {
				i = reader.nextInt();
				if (i >= 0 && i <= 3) {
					numcorrecte = true;
				} else {
					System.out.print("El n�mero de no �s correcte ha de ser entre 0 i 3 , introdueix un altre: ");
				}
			} else {
				System.out.print("Ha de ser un n�mero enter, introdueix un altre: ");
				reader.next();
			}
		}
		return i;
	
	}
	/**
	 * Aquesta funci� comprova que el n�mero de casella/fila no estigui destapat ja pr�viament.
	 * @param fila �s el n�mero de fila a on vol destapar la casella l'usuari
	 * @param columna �s el n�mero de columna a on vol destapar la casella l'usuari
	 * @param tauler �s la matriu de la casella per comprobar que encara hi hagi un interrogant.
	 * @return Torna un boole�, true en cas de que la casella encara estigui destapada, i false si ja casella ja est� destapada.
	 */
	static boolean validarCasella(int fila, int columna,char[][]tauler) {
		boolean casellaok = false;
		if(tauler[fila][columna]=='?') {
			casellaok=true;
		}else {
			System.out.println("Aquesta casella ja est� destapada!");
		}
		return casellaok;
	}
	/**
	 * Funci� que gestiona els torns dels dos jugadors cridant a altres funcions i amb bucles.
	 * @param tauler Reb el tauler actual amb les caselles amb interrogants o destapades depenent del punt de la partida.
	 * @param secret Reb el la matriu secret ja remenada.
	 * @return Retorna un boole� depenent si l'usuari ha encertat la parella o no: true si l'ha encertat, false si no l'ha encertat.
	 */
	static boolean torn(char[][]tauler, char[][]secret) {
		int fila=0;
		int columna=0;
		boolean casellaok=false;
		boolean coincideix=false;
		System.out.println("Primera tirada ...");
		while(!casellaok) {
			System.out.println("Introduiex la fila: ");
			fila=validarDada();
			System.out.println("Introduiex la columna: ");
			columna=validarDada();
			casellaok=validarCasella(fila,columna,tauler);
		}
		tauler[fila][columna]=secret[fila][columna];
		mostraTauler(tauler);
		int fila2=0;
		int columna2=0;
		casellaok=false;
		System.out.println("Segona tirada...");
		while(!casellaok) {
			System.out.println("Introduiex la fila: ");
			fila2=validarDada();
			System.out.println("Introduiex la columna: ");
			columna2=validarDada();
			casellaok=validarCasella(fila2,columna2,tauler);
		}
		tauler[fila2][columna2]=secret[fila2][columna2];
		mostraTauler(tauler);
		if(tauler[fila][columna]==tauler[fila2][columna2]) {
			coincideix=true;
		}else {
			tauler[fila][columna]='?';
			tauler[fila2][columna2]='?';
		}
		return coincideix;
	}
}