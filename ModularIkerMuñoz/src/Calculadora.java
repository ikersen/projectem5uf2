import java.util.Scanner;
public class Calculadora {
	static int num1 = 0;
	static int num2 = 0;
	static int resul = 0;
	static boolean numsintroduits = false;
	static boolean accio = false;
	static Scanner reader = new Scanner(System.in);

	public static void main(String[] args) {
		
		int opcio=1;
		while(opcio!=0) {
			mostraMenu();
			opcio=llegirNum();
			realitzaAccio(opcio);
		}
		
		
		
	}

	static void mostraMenu() {
		System.out.println("Aix� �s una calculadora, primer et demanar� dos n�meros a operar i despr�s la operaci� a fer");
		System.out.println("1. Obtenir dos n�meros a operar");
		System.out.println("2. Sumar");
		System.out.println("3. Restar");
		System.out.println("4. Multiplicar");
		System.out.println("5. Divisi�");
		System.out.println("6. Visualitzar el resultat");
		System.out.println("0. Sortir del programa");

	}

	static int llegirNum() {
		int numficat=0;
		boolean numcorrecte=false;
		while(!numcorrecte) {
			if(reader.hasNextInt()) {
				numficat=reader.nextInt();
				numcorrecte=true;
			}else {
				System.out.println("Ha de ser un n�mero enter, introdueix un altre v�lid: ");
				reader.next();
			}
			
		}
		return numficat;

	}
	
	static void realitzaAccio(int opcioescollida) {
		switch (opcioescollida){
			case 0:
				System.out.print("Aqu� acaba el programa");
				break;
			case 1:
				obtenirNums();
				break;
			case 2:
				suma(num1, num2);
				break;
			case 3:
				resta(num1, num2);
				break;
			case 4:
				multiplicacio(num1, num2);
				break;
			case 5:
				divisio(num1, num2);
				break;
			case 6:
				visualitzaResultats();
				break;
			default:
				System.out.println("No �s una opci� v�lida");
		}

	}
	
	static void obtenirNums() {
		System.out.println("Introdueix el n�mero 1");
		boolean numcorrecte=false;
		while(!numcorrecte) {
			if(reader.hasNextInt()) {
				num1=reader.nextInt();
				numcorrecte=true;
			}else {
				System.out.println("Ha de ser un n�mero enter, introdueix un altre v�lid: ");
				reader.next();
			}
			
		}
		
		System.out.println("Introdueix el n�mero 2");
		numcorrecte=false;
		while(!numcorrecte) {
			if(reader.hasNextInt()) {
				num2=reader.nextInt();
				numcorrecte=true;
			}else {
				System.out.println("Ha de ser un n�mero enter, introdueix un altre v�lid: ");
				reader.next();
			}
			
		}
		numsintroduits=true;
		
	}
	
	static void suma(int a, int b) {
		if(numsintroduits) {
			resul=a+b;
			accio=true;
		}else {
			System.out.println("Has de introduir els dos n�meros pr�viament amb la opci� 1!");
		}
		
	}
	
	static void resta(int a, int b) {
		if(numsintroduits) {
			resul=a-b;
			accio=true;
		}else {
			System.out.println("Has de introduir els dos n�meros pr�viament amb la opci� 1!");
		}
	}
	static void multiplicacio(int a, int b) {
		if(numsintroduits) {
			resul=a*b;
			accio=true;
		}else {
			System.out.println("Has de introduir els dos n�meros pr�viament amb la opci� 1!");
		}
		
	}
	static void divisio(int a, int b) {
		if(numsintroduits) {
			if(b!=0) {
				System.out.println("Vols obtenir el m�dul o la divis� entera(modul|divisio)");
				String eleccio=reader.next();
				if(eleccio.equals("modul")) {
					resul=a%b;
					accio=true;
				}else if(eleccio.equals("divisio")) {
					resul=a/b;
					accio=true;
				}else {
					System.out.println("Has escollit malament, n�mes hi han aquestes dues opcions");
				}
				
			}else {
				System.out.println("El divisor no pot ser 0 a una divisi�!");
			}
			
		}else {
			System.out.println("Has de introduir els dos n�meros pr�viament amb la opci� 1!");
		}
		
	}
	
	static void visualitzaResultats() {
		if(numsintroduits) {
			if(accio) {
				System.out.println("El resultat �s: "+resul);
			}else {
				System.out.println("Has de fer pr�viament una operaci�!");
			}		
		}else {
			System.out.println("Has de introduir els dos n�meros pr�viament amb la opci� 1!");
		}
	}
}
