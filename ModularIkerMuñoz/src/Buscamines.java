import java.util.Scanner;

/**
 * Un programa que representa el joc popular del Buscamines.
 * 
 * @version 1
 * @author Iker Mu�oz
 */
public class Buscamines {
	static Scanner reader = new Scanner(System.in);
	/**
	 * Declaraci� com a variable global del tauler secret de mines.
	 */
	static int mines[][] = new int[0][0];
	/**
	 * Declaraci� com a variable global del camp.
	 */
	static int camp[][] = new int[0][0];
	/**
	 * Declaraci� com a variable global, la variable que controla les caselles que
	 * encara queden lliures de mines.
	 */
	static int caselleslliures = 0;
	/**
	 * Declaraci� com a variable global el vector a on es guarden els noms dels
	 * campions, amb tamany de 5.
	 */
	static String[] ranking = new String[5];

	/**
	 * El main cridar� a la funci� joc que ser� la que gestionar� el funcionament
	 * general.
	 */

	public static void main(String[] args) {
		joc();

	}

	/**
	 * La funci� joc gestiona el joc sencer.
	 * 
	 * Inicialitza variables i crida a altres funcions per al correcte funcionament
	 * del joc.
	 */
	static void joc() {
		int opcio = 0;
		boolean opcioescollida = false;
		String nom = "";
		int contadorguanyadors = 0;

		do {
			// Mostrem el men�
			System.out.println("\n\nMen� d'opcions: ");
			System.out.println("1. Mostrar ajuda");
			System.out.println("2. Opcions");
			System.out.println("3. Jugar Partida");
			System.out.println("4. Veure Rankings");
			System.out.println("0. Sortir");
			System.out.print("Introdueix una opci�: ");

			boolean numcorrecte = false;

			while (!numcorrecte) {
				if (reader.hasNextInt()) {
					opcio = reader.nextInt();
					numcorrecte = true;
				} else {
					System.out.print("Ha de ser un n�mero enter, introdueix un altre: ");
					reader.next();
				}
			}

			switch (opcio) {

			case 0: // Sortir
				System.out.println("Adeu!");
				break;
			case 1:
				mostraAjuda();
				break;
			case 2:
				nom = opcions(nom);
				opcioescollida = true;
				break;
			case 3:
				if (opcioescollida) {
					jugar(nom, contadorguanyadors);
				} else {
					System.out.println("Has de pasar pr�viament per la opci� 2 per poder jugar!");
				}
				opcioescollida = false;
				break;
			case 4:
				ranking();
				break;

			default:
				System.out.println("Opci� incorrecta, ha d'estar entre 0 i 4");
				break;
			}
		} while (opcio != 0);

	}

	/**
	 * La funci� �s cridada per mostrar la ajuda sobre el joc.
	 */
	static void mostraAjuda() {
		System.out.println("El joc consisteix a buidar totes les caselles d'una pantalla que no ocultin una mina.\r\n"
				+ "Algunes caselles tenen un nombre, el qual indica la quantitat de mines que hi ha a les caselles circumdants.\r\n"
				+ "Si es descobreix una casella amb una mina es perd la partida."
				+ "\r\nPer jugar haur�s de passar per la opci� 2 i dir el teu nom, el tamany del tauler i el nombre de mines, posteriorment"
				+ " \r\nhaur�s de fer servir la opci� 3 per jugar, i la 4 per veure el nom dels jugadors que hagin superat"
				+ "\r\nel Buscamines." + "\r\nLa opci� 0 acabar� el programa. Sort! ");

	}

	/**
	 * La funci� �s cridada per a que l'usuari configuri les opcions del joc, el seu
	 * nom, el n�mero de files, columnes i mines.
	 * 
	 * @param nom �s el nom que l'usuari ficar� com el nom seu al joc.
	 * @return Retorna el nom que ha introduit l'usuari.
	 */
	static String opcions(String nom) {
		int files = 0;
		int columnes = 0;
		int nummines = 0;
		System.out.print("Escriu el teu nom: ");
		nom = reader.next();
		System.out.print("Escriu el nombre de files que vols que tingui el tauler, (m�nim 4, m�xim 25): ");
		boolean numcorrecte = false;
		while (!numcorrecte) {
			if (reader.hasNextInt()) {
				files = reader.nextInt();
				if (files >= 26 || files <= 3) {
					System.out.print("Les files han de ser de m�nim 4, m�xim 25: ");
				} else {
					numcorrecte = true;
				}

			} else {
				System.out.print("Ha de ser un n�mero enter, introdueix un altre: ");
				reader.next();
			}
		}
		System.out.print("Escriu el nombre de columnes que vols que tingui el tauler, (m�nim 4, m�xim 25): ");
		numcorrecte = false;
		while (!numcorrecte) {
			if (reader.hasNextInt()) {
				columnes = reader.nextInt();
				if (columnes >= 26 || columnes <= 3) {
					System.out.print("Les columnes han de ser de m�nim 4, m�xim 25: ");
				} else {
					numcorrecte = true;
				}
			} else {
				System.out.print("Ha de ser un n�mero enter, introdueix un altre: ");
				reader.next();
			}
		}
		System.out.print(
				"Escriu el nombre de mines que hi hagi al tauler, han de ser entre un 50% i 80% de les caselles disponibles: ");
		numcorrecte = false;
		while (!numcorrecte) {
			if (reader.hasNextInt()) {
				nummines = reader.nextInt();
				if (nummines < (files * columnes) * 0.5 || nummines > (files * columnes) * 0.8) {
					System.out.print("Les mines han de ser entre un 50% i 80% de les caselles disponibles: ");
				} else {
					numcorrecte = true;
				}
			} else {
				System.out.print("Ha de ser un n�mero enter, introdueix un altre: ");
				reader.next();
			}
		}
		caselleslliures = files * columnes - nummines;
		inicialitzarMines(files, columnes, nummines);
		inicialitzarCamp(files, columnes);
		return nom;
	}

	/**
	 * La funci� col�loca mines tenint en compte les opcions pr�viament introduides.
	 * 
	 * @param files    Cont� les files que tindr� la matriu.
	 * @param columnes Cont� les columnes que tindr� la matriu.
	 * @param nummines Cont� les n�mero de mines que tindr� la matriu.
	 */
	static void inicialitzarMines(int files, int columnes, int nummines) {
		mines = new int[files][columnes];
		for (int f = 0; f < files; f++) {
			for (int c = 0; c < columnes; c++) {
				mines[f][c] = 0;
			}
		}

		int max = 0;
		int min = 0;
		int range = 0;

		while (nummines > 0) {
			max = files - 1;
			min = 0;
			range = max - min + 1;
			int fila = (int) (Math.random() * range) + min;
			max = columnes - 1;
			min = 0;
			range = max - min + 1;
			int columna = (int) (Math.random() * range) + min;
			if (mines[fila][columna] == 1) {
				nummines++;
			}
			mines[fila][columna] = 1;
			nummines--;
		}

		
	}
	/**
	 * La funci� col�loca tot el camp ple de 9.
	 * 
	 * @param files    Cont� les files que tindr� la matriu.
	 * @param columnes Cont� les columnes que tindr� la matriu.
	 */

	static void inicialitzarCamp(int files, int columnes) {
		camp = new int[files][columnes];
		for (int f = 0; f < files; f++) {
			for (int c = 0; c < columnes; c++) {
				camp[f][c] = 9;
			}
		}

	}

	/**
	 * La funci� mostra el camp amb un format amigable per al usuari.
	 * 
	 */
	static void visualitzarCamp() {
		int i = 0;
		System.out.print("C   ");
		while (i < camp[0].length) {
			if (i >= 10) {
				System.out.print(i + " ");
			} else {
				System.out.print(i + "  ");
			}

			i++;
		}
		System.out.print("\nF\n");
		for (int f = 0; f < camp.length; f++) {
			if (f >= 10) {
				System.out.print(f + "  ");
			} else {
				System.out.print(f + "   ");
			}
			for (int c = 0; c < camp[f].length; c++) {
				System.out.print(camp[f][c] + "  ");
			}
			System.out.println();
		}

	}
	/**
	 * La funci� controla la opci� de Jugar, acaba quan estalla la mina o si ja no queden caselles per destapar lliures de mines.
	 * @param nom �s el nom del jugador.
	 * @param contadorguanyadors Conta quants jugadors han sigut capa�os de guanyar.
	 */
	static void jugar(String nom, int contadorguanyadors) {
		int f = 0;
		int c = 0;
		boolean partidaEnCurs = true;
		while (partidaEnCurs == true) {
			visualitzarCamp();
			f = demanaF(nom);
			c = demanaC(nom);
			descobrir(f, c);
			if (camp[f][c] == 10) {
				System.out.println("HA ESTALLAT LA MINA!");
				visualitzarCamp();
				partidaEnCurs = false;
			} else if (caselleslliures == 1) {
				System.out.println("HAS GUANYAT!" + nom + " ENHORABONA");
				partidaEnCurs = false;
				omplirRanking(nom, contadorguanyadors);
			}
			caselleslliures--;
		}
	}

	/**
	 * La funci� demana a l'usuari que introduixi una fila per destapar.
	 * @param nom �s el nom del jugador.
	 * @return Retorna la fila introduida.
	 */
	static int demanaF(String nom) {
		int f = 0;
		System.out.print(nom + ", escriu la fila a destapar: ");
		boolean numcorrecte = false;
		while (!numcorrecte) {
			if (reader.hasNextInt()) {
				f = reader.nextInt();
				if (f >= camp.length || f < 0) {
					System.out.print("Ha de ser una fila que estigui al camp, torna a intentar-ho: ");
				} else {
					numcorrecte = true;
				}
			} else {
				System.out.print("Ha de ser un n�mero enter, introdueix un altre: ");
				reader.next();
			}
		}
		return f;
	}

	/**
	 * La funci� demana a l'usuari que introduixi una columna per destapar.
	 * @param nom �s el nom del jugador.
	 * @return Retorna la columna introduida.
	 */
	static int demanaC(String nom) {
		int c = 0;
		System.out.print(nom + ", escriu la columna a destapar: ");
		boolean numcorrecte = false;
		while (!numcorrecte) {
			if (reader.hasNextInt()) {
				c = reader.nextInt();
				if (c >= camp[0].length || c < 0) {
					System.out.print("Ha de ser una columna que estigui al camp, torna a intentar-ho: ");
				} else {
					numcorrecte = true;
				}
			} else {
				System.out.print("Ha de ser un n�mero enter, introdueix un altre: ");
				reader.next();
			}
		}
		return c;
	}

	/**
	 * La funci� controla si hi ha mina en la casella escollida per el usuari, en cas de que ni hi hagi mina compta les mines que hi han al voltant seu .
	 * Aquest n�mero del voltant de mines o si hi ha mina el fica a la casella escollida del camp
	 * @param f �s la fila escollida per el usuari.
	 * @param c �s la columna escollida per el usuari.
	 */
	static void descobrir(int f, int c) {
		int d = 0;
		if (mines[f][c] == 1) {
			camp[f][c] = 10;
		} else {
			if (c + 1 < mines[f].length && mines[f][c + 1] == 1) {
				d++;

			}
			if (c - 1 >= 0 && mines[f][c - 1] == 1) {
				d++;

			}
			if (f - 1 >= 0 && mines[f - 1][c] == 1) {
				d++;

			}
			if (f + 1 < mines.length && mines[f + 1][c] == 1) {
				d++;

			}
			if (f - 1 >= 0) {
				if (c + 1 < mines[f].length && mines[f - 1][c + 1] == 1) {
					d++;
				}

			}
			if (f + 1 < mines.length) {
				if (c + 1 < mines[f].length && mines[f + 1][c + 1] == 1) {
					d++;
				}

			}
			if (f - 1 >= 0) {
				if (c - 1 >= 0 && mines[f - 1][c - 1] == 1) {
					d++;
				}

			}
			if (f + 1 < mines.length) {
				if (c - 1 >= 0 && mines[f + 1][c - 1] == 1) {
					d++;
				}
			}

			camp[f][c] = d;
		}

	}

	/**
	 * La funci� fica en un vector el nom dels guanyadors amb un m�xim de 5 .
	 * @param nom �s el nom del usuari.
	 * @param contadorguanyadors �s el recompte de guanyadors, quan arriba a 5 passa a 0.
	 */
	static void omplirRanking(String nom, int contadorguanyadors) {
		ranking[contadorguanyadors] = nom;
		contadorguanyadors++;
		if (contadorguanyadors == 5) {
			contadorguanyadors = 0;
		}

	}

	/**
	 * La funci� mostra el nom dels guanyadors.
	 */
	static void ranking() {
		System.out.println("Els jugadors que han superat els buscamines s�n: ");
		for (int i = 0; i < ranking.length; i++) {
			System.out.print(ranking[i] + ", ");
		}

	}

}
