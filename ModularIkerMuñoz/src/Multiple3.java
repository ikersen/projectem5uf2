import java.util.Scanner;

public class Multiple3 {

	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);

		System.out.println("Et dire quants n�meros m�ltiples i quins hi ha entre 3 i 100");
		int sum=0;
		
		for(int i=1 ;i<=100 ;i++) {
			if(i%3==0) {
				sum++;
				System.out.print(i +" ");
			}
		}
		System.out.println();
		System.out.print("Hi ha "+sum+" n�meros");

		reader.close();
	}
}
